import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Budget } from '../entity/budget';

@Injectable({
  providedIn: 'root'
})
export class BudgetService {
  private url = 'http://localhost:8080/api/budget/';

  constructor(private http:HttpClient) { }

  findAll():Observable<Budget[]> {
    return this.http.get<Budget[]>(this.url);
  }

  find(id:number): Observable<Budget> {
    return this.http.get<Budget>(this.url + id);
  }

  add(budget:Budget): Observable<Budget> {
    return this.http.post<Budget>(this.url, budget);
  }

  update(budget:Budget): Observable<Budget> {
    return this.http.put<Budget>(this.url + budget.id, budget);
  }

  delete(id:number): Observable<any> {
    return this.http.delete(this.url + id);
  }

}