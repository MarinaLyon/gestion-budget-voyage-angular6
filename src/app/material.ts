import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";

/**
 * On peut faire un petit tableau à part dédié aux imports des
 * modules de material design, histoire de garder le AppModule un peu
 * plus lisible
 */
export const matModules = [
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule
];
