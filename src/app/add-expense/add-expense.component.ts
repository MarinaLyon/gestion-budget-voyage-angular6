import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Expense } from '../entity/expense';



@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.css']
})
export class AddExpenseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
