import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Budget } from '../entity/budget';
import { BudgetService } from '../service/budget.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-budget',
  templateUrl: './add-budget.component.html',
  styleUrls: ['./add-budget.component.css']
})
export class AddBudgetComponent implements OnInit {

  form: FormGroup;
  //j'injecte le budgetservice (voir angular contact message.component.ts)
  //j'injecte le Router
  budget:Budget = {amount:null, travel:""};
  
  constructor(private formBuilder: FormBuilder, private budgetService:BudgetService, private router:Router) {
    this.initForm()
  }

  initForm() { 
    // initialisation du formulaire 
    this.form = this.formBuilder.group({ 
      amount: ['', ],
      travel: ['', ]
    }); 
  } 
 
  ngOnInit() { 
  } 
 
  onSubmit() { 
    //Je donne le form.value au add du budget service et je subscribe dessus
    this.budgetService.add(this.form.value)
    .subscribe( data =>
      //dans le subscribe, je fais un router.navigate et je mets deux param : la route et l'id 
      this.router.navigate(['/add-expense', data.id])
    )
  } 
} 
