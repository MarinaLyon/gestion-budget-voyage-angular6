import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { matModules } from './material';
import { HomeComponent } from './home/home.component';
import { AddBudgetComponent } from './add-budget/add-budget.component';
import { AddExpenseComponent } from './add-expense/add-expense.component';
import { ExpenseDetailsComponent } from './expense-details/expense-details.component';
import { RemainBudgetComponent } from './remain-budget/remain-budget.component';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NewExpenseComponent } from './new-expense/new-expense.component';



const routes: Routes = [
  
  {path: 'add-budget', component: AddBudgetComponent },
  {path: 'add-expense/:id', component: AddExpenseComponent },
  {path: 'expense-details', component: ExpenseDetailsComponent },
  {path: 'home', component: HomeComponent },
  {path: 'register', component:RegisterComponent},
  {path: 'remain-budget', component: RemainBudgetComponent },

  {path: '', redirectTo:'/register', pathMatch:'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HomeComponent,
    AddBudgetComponent,
    AddExpenseComponent,
    ExpenseDetailsComponent,
    RemainBudgetComponent,
    NewExpenseComponent,
   
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    matModules,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    NgbModule,
    NgbModule.forRoot()
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }