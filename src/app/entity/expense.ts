export interface Expense {
  id?:number;
  cost:number;
  category:string;
  label:string;
  date:string;
  budget_id?:number;
}
