export interface Budget {
  id?:number;
  amount:number;
  travel:string;
  expenses?:number;
}
