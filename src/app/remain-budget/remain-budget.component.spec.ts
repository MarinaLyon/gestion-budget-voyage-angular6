import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemainBudgetComponent } from './remain-budget.component';

describe('RemainBudgetComponent', () => {
  let component: RemainBudgetComponent;
  let fixture: ComponentFixture<RemainBudgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemainBudgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemainBudgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
